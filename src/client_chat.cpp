#include "clientlibrary.h"

using namespace std;

/*******************************************************************
*   FUNÇÃO P/ RECEBER E MOSTRAR MENSAGENS VINDAS DO SERVIDOR
*******************************************************************/
/**
 * recebe mensagens do servidor
 * mostra mensagens recebidas do servidor
*/

void * receiveMessageServer(void * args) {   
    int clientSd = *((int *) args);

    char msg[1500];
    while(1) {
        int bytesRead = 0;
        memset(&msg, 0, sizeof(msg));   /**apagar o buffer**/
        bytesRead += recv(clientSd, (char*)&msg, sizeof(msg), 0);
        if(!strcmp(msg, "exit")){
            cout << "O servidor encerrou a sessão" << endl;
            break;
        }
        cout << msg << endl;
    }
    return NULL;
}

int main(int argc, char *argv[]){
    if(argc != 4){
        cerr << "Uso: nome do cliente endereço de IP porta" << endl; exit(0); 
    } //endereço de IP e o numero da porta  
    char *serverIp = argv[2]; int port = atoi(argv[3]); 
    char * nomeCliente = argv[1];
    //cria o buffer de mensagem
    char buffer[1500]; 
    //configura o socket 
    struct hostent* host = gethostbyname(serverIp); 
    sockaddr_in sendSockAddr;   
    bzero((char*)&sendSockAddr, sizeof(sendSockAddr)); 
    sendSockAddr.sin_family = AF_INET; 
    sendSockAddr.sin_addr.s_addr = 
        inet_addr(inet_ntoa(*(struct in_addr*)*host->h_addr_list));
    sendSockAddr.sin_port = htons(port);
    int clientSd = socket(AF_INET, SOCK_STREAM, 0);
    int status = connect(clientSd,
                         (sockaddr*) &sendSockAddr, sizeof(sendSockAddr));
    if(status < 0){
        cout<<"Erro de conexao com socket!"<<endl;
        exit(1);
    }
    
    char nome[25];
    strcpy(nome,nomeCliente);
    //envia nome para o servidor
    send(clientSd, (char*)&nome, sizeof(nome), 0);

    char respConn[100];
    memset(&respConn, 0, sizeof(respConn));//apagar o buffer
    recv(clientSd, (char*)&respConn, sizeof(respConn), 0);
    char msgSucesso[] = "S";
    if (!strcmp(respConn, msgSucesso)) {
        cout << "Conexão estabelecida com sucesso!" << endl;
    } else {
        cout << respConn << endl;
        exit(1);
    }
    int bytesRead, bytesWritten = 0;

    //thread para receber sempre mensagens do server
    pthread_t threadParaMsg;
    pthread_create(&threadParaMsg, NULL, recebeMensagemServer, &clientSd);

    while(1){
        //recebe dado de entrada
        
        string data;
        getline(cin, data);
        if (data.length() > 1500) {
            cout << "ERRO: O tamanho da mensagem é maior que 1500 caracteres" << endl;
            continue;
        }

        memset(&buffer, 0, sizeof(buffer));//limpa o buffer
        strcpy(buffer, data.c_str());
        if(data == "exit"){
            cout << "Fim da conexão" << endl;
            send(clientSd, buffer, 1500, 0);
            break;
        }
        send(clientSd, buffer, 1500, 0);
    }
    pthread_join(threadParaMsg, NULL);

    close(clientSd);
    cout << "Fim da conexão" << endl;
    return 0;    
}
