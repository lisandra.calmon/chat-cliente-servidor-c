#include "serverlibrary.h"
using namespace std;
/*******************************************************************
*   TYPEDEFS
*******************************************************************/
typedef struct{
    string nome;
	int index;
	int sockID;
	struct sockaddr_in clientAddr;
	int len;

} client;

vector<client> clientes;

/*******************************************************************
*   GLOBAL VARIABLES
*******************************************************************/
vector<pthread_t> threads;
vector<int>::iterator it;

int qtdThreads;
int qtdAtualClientes;


/*******************************************************************
*   FUNCTION DE ENVIO DE MENSAGEM DIRETA
*******************************************************************/
/**
 * @param buffer - contem a mensagem
 * @param clienteEmissor - struct de cliente que solicita o envio
*/
bool sendMessage(char * buffer, client clienteEmissor) {
    for (client cliente : clientes) {
        /*tratativa de validação para não haver envio para si mesmo 
        * não envia para o cliente emissor, apenas para os outros*/
        if ((cliente.nome).compare(clienteEmissor.nome) != 0) {
            send(cliente.sockID, buffer, strlen(buffer), 0);
        }
    }
    return true;
}

/*******************************************************************
*   FUNCTION DE ENVIO DE MENSAGEM DIRETA
*******************************************************************/
/**
 * @param buffer - contem a mensagem
 * @param clienteEmissor - struct de cliente que solicita o envio
 * @param clienteEmissor - struct de cliente que solicita o envio
*/
bool sendMessageTo(char * buffer, string nomeClienteReceptor, client clienteEmissor) {
    for (client cliente : clientes) {
        //procura o receptor pelo nome e envia a mensagem para ele
        if ((cliente.nome).compare(nomeClienteReceptor) == 0) {
            send(cliente.sockID, buffer, strlen(buffer), 0);
            return true;
        }
    }
    return false;
}

bool whoIsConnected(client quemPergunta) {
    char clientesConectados[2000];
    memset(&clientesConectados, 0, sizeof(clientesConectados));
    for (client cliente : clientes) {
        char * name = new char(cliente.nome.length()+1);
        memcpy(name,cliente.nome.c_str(), cliente.nome.length() + 1);
        strcat(clientesConectados,name);
        strcat(clientesConectados,"\n");
    }
    send(quemPergunta.sockID, clientesConectados, strlen(clientesConectados), 0);
    return true;
}

bool helpMe(client quemPrecisa) {
    
    //aqui constroi char * com todos os comandos e como usar
    char comandos[2000];
    strcpy(comandos, "SEND <MESSAGE>: Envia <MESSAGE> para todos os clientes conectados.\n");
    strcat(comandos,"SENDTO <CLIENT_NAME> <MESSAGE>: Envia <MESSAGE> para o cliente <CLIENT_NAME>\n");
    strcat(comandos,"WHO: Retorna a lista dos clientes conectados ao servidor\n");
    strcat(comandos,"HELP: Retorna a lista de comandos suportados e seu uso\n");
    //e então envia
    send(quemPrecisa.sockID, comandos, strlen(comandos), 0);
    return true;
}

void * connectClient(void * args) {

    client* cliente = (client*) args;

    string nome = cliente->nome;
    int sockID = cliente->sockID;

    int bytesRead = 0;
    int bytesWritten = 0;
    char buffer[1500];
    
    char comando[10];

    while(1){
        bool result = false;
        struct tm *horario;
        time_t segundos;
        time(&segundos);
        horario = localtime(&segundos);
        //recebe uma mensagem do cliente (listen)
        memset(&buffer, 0, sizeof(buffer));//limpa o buffer
        int err = recv(sockID, buffer, 2020, 0);
        if(err == -1){
            cout << "Não foi possivel fazer a leitura do buffer"<< endl;
        }
        
        if(!strcmp(buffer, "exit")){
            cout << "Client saiu da sessão" << endl;
            break;
        }

        char * palavra = strtok(buffer, " ");
        strcpy(comando, palavra);

        if (strcmp(comando,"SEND") == 0) {

            char msg[2020];
            memset(&msg, 0, sizeof(msg));
            for ( int i = 0; i < nome.length(); i++) {
                msg[i] = nome[i];
            }
            msg[nome.length()] = ':';
            msg[nome.length() + 1] = ' ';
            while(1) {
                palavra = strtok(NULL, " ");
                if(palavra == NULL) {
                    break;
                }
                strcat(msg, palavra);
                strcat(msg, " ");
            }
            result = sendMessage(msg,*cliente);

        } else if (strcmp(comando, "SENDTO") == 0) {
            char nomeDest[40];
            palavra = strtok(NULL, " ");
            
            if (palavra == NULL || strcmp(palavra,"") != 0) {
                char msgErro[] = "Erro: Informe o comando, o nome do destinatário e a mensagem";
                send(cliente->sockID, msgErro, strlen(msgErro), 0);
                continue;
            }
            
            //salvo nome do destinatário            
            strcpy(nomeDest,palavra);

            char msg[2020];
            memset(&msg, 0, sizeof(msg));
            for ( int i = 0; i < nome.length(); i++) {
                msg[i] = nome[i];
            }
            msg[nome.length()] = ':';
            msg[nome.length() + 1] = ' ';
            while(1) {
                palavra = strtok(NULL, " ");
                if(palavra == NULL) {
                    break;
                }
                strcat(msg, palavra);
                strcat(msg, " ");
            }
            result = sendMessageTo(msg, nomeDest, *cliente);     

        } else if (strcmp(comando, "WHO") == 0) {
            result = whoIsConnected(*cliente);

        } else if (strcmp(comando, "HELP") == 0) {
            result = helpMe(*cliente);

        } else {
            char msgErro[] = "Erro: comando não encontrado\n";
            send(cliente->sockID, msgErro, strlen(msgErro), 0);
        }

        cout << horario->tm_hour <<":"<< horario->tm_min << "   " << nome << " "<< comando << " Executado: ";
        if (result) {
            cout << "Sim" << endl;
        } else {
            cout << "Não" << endl;
        }
    }
    return NULL;
}
